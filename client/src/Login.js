import React, { Component } from 'react';
import { Redirect } from 'react-router-dom';
import { Alert, Container, Row, Col, Button, Form, FormGroup, Label, Input } from 'reactstrap';
import { PostData } from './Utils';

class Login extends Component {
    constructor(props) {
      super(props);

      this.state = {
        errors: false,
        errorDetails: false,
        isLoaded: false,
        items: [],
        user: '',
        password: '',
        showDetails: false
      };

      this.handleChange = this.handleChange.bind(this);
      this.handleSubmit = this.handleSubmit.bind(this);

      // localStorage.setItem('authToken', '');
    }

    validateForm() {
      return this.state.user.length > 0 && this.state.password.length > 5;
    }

    handleChange(event) {
      this.setState({
        [event.target.id]: event.target.value,
        errors: false,
        errorDetails: ''
      });
    }

    handleSubmit(event) {
      event.preventDefault();

      PostData('http://localhost:9001/auth/', {
        user: this.state.user,
        password: this.state.password
      }).then(data => {
          localStorage.setItem('authToken', '');

          if (data && data.authToken) {
            localStorage.setItem('authToken', data.authToken);

            this.setState({
              isLoaded: true,
              items: data.items,
              showDetails: true,
              errors: false
            });
          }

          if (data.error) {
            this.setState({
              isLoaded: true,
              errors: true,
              showDetails: false,
              errorDetails: 'Incorrect Username or Password. Please try again!'
            });
          }

        }).catch(reason => {

          this.setState({
            isLoaded: true,
            errors: true,
            showDetails: false,
            errorDetails: 'Incorrect Username or Password. Please try again!'
          });

          localStorage.setItem('authToken', '');

        });
    }

    render() {
      if (this.state.showDetails) {
        return (
          <Redirect to='/details/' />
        )
      }

      return (
        <Container>
          <Row>
             <Col sm='12' md={{ size: 8, offset: 2 }}>
              <Form onSubmit={this.handleSubmit}>

                  <FormGroup row>
                    { this.state.errors ?
                      <Alert color='danger'>
                        Incorrect Username or Password. Please try again!
                      </Alert> : ''
                    }
                  </FormGroup>
                  <FormGroup row>
                    <Label for='user'>Username</Label>
                    <Input type='text' name='user' id='user'
                            placeholder='Enter your username'
                            value={this.state.user}
                            onChange={this.handleChange} onFocus={this.handleFocusBlur} valid />
                  </FormGroup>

                  <FormGroup row>
                    <Label for='password'>Password</Label>
                    <Input type='password' name='password' id='password'
                            placeholder='Enter your password'
                            value={this.state.password} onChange={this.handleChange} valid />
                  </FormGroup>

                  <FormGroup row>
                    <Button>Submit</Button>
                  </FormGroup>
              </Form>
            </Col>
          </Row>
        </Container>
      );
    }
};

export default Login;
