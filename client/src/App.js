import React, { Component } from 'react';
import {BrowserRouter as Router} from 'react-router-dom'
import Main from './Main';

import 'bootstrap/dist/css/bootstrap.min.css';
require('dotenv').config();

class App extends Component {
  render() {
    return (
      <div className='app'>
        <Router>
          <Main />
        </Router>
      </div>
    );
  }
}


export default App;
