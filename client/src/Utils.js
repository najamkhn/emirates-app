const FetchData = async function fetchData(url) {
  let response = await fetch(url);
  let data = await response.json();
  return data;
};

const PostData = async function postData(url, postData, callback) {
  console.log(postData);
  let postDataStr = JSON.stringify(postData);
  let response = await fetch(url, {
    method: 'POST',
    body: postDataStr,
    headers: {'content-type': 'application/json'}
  });

  let data = await response.json();
  return data;
}

export {FetchData, PostData};
