import React, { Component } from 'react';
import { Table } from 'reactstrap';
import { FetchData } from './Utils';

class ReadingPreferences extends Component {
    constructor(props) {
      super(props);
      this.state = {
        error: null,
        isLoaded: false,
        items: []
      };
    }

    componentDidMount() {
      FetchData('https://www.googleapis.com/books/v1/volumes?q=isbn:0747532699')
        .then(data => this.setState({
            isLoaded: true,
            items: data.items
          })
        )
        .catch(reason => this.setState({
            isLoaded: true,
            error: true
          })
        );
    }

    render() {
      const { error, isLoaded, items } = this.state;
      if (error) {
        return <div>Error: {error.message}</div>;
      } else if (!isLoaded) {
        return <div>Loading...</div>;
      } else {
        return (
          <Table>
            <thead>
              <tr>
                <th>Book Name</th>
                <th>Author Name</th>
                <th>Publisher Name</th>
              </tr>
            </thead>
            <tbody>
              {items.map(item => (
                <tr key='{item.toString()}'>
                  <td>{item.volumeInfo.title}</td>
                  <td>{item.volumeInfo.authors}</td>
                  <td>{item.volumeInfo.publisher}</td>
                </tr>
              ))}
            </tbody>
          </Table>
        );
      }
  }
}

export default ReadingPreferences;
