import React, { Component } from 'react';
import { Redirect } from 'react-router-dom';
import { Container, Row, Col, Button, Form, FormGroup, Label, Input } from 'reactstrap';
import ReadingPreferences from './ReadingPreferences';

const jwt = require('jsonwebtoken');

class Details extends Component {

  constructor(props) {
    super(props);
    this.state = {
      viewTable: false,
      user: '',
      emailID: '',
      address: '',
      error: false
    };

    this.showTable = this.showTable.bind(this);
    this.hideTable = this.hideTable.bind(this);
  }

  verifyToken() {
    jwt.verify(localStorage.getItem('authToken'), process.env.REACT_APP_SECRETKEY, (err, decoded) => {
      if (!decoded) {
        this.setState({
          error: true
        });
        return false;
      }

      this.setState({
        user: decoded.username,
        emailID: decoded.email,
        address: decoded.zip
      });

      return true;

    });
  }

  showTable(event) {
    event.preventDefault();
    this.readingPreferences = <ReadingPreferences />;
    this.setState({viewTable: true});
  }

  hideTable(event) {
    event.preventDefault();
    this.setState({viewTable: false});
  }

  componentWillMount() {
    this.verifyToken();
  }

  render() {
    if (!localStorage.getItem('authToken') || this.state.error) {
      return (
        <Redirect to="/login" />
      )
    }

    return (
      <Container>
        <Row>
           <Col sm='12' md={{ size: 8, offset: 2 }}>
            <Form>
              <FormGroup row>
                <Label for='idUsername' sm={2}>Username</Label>
                <Col sm={10}>
                  <Input type='text' name='username' id='idUsername' placeholder='N/A' disabled value={this.state.user} />
                </Col>
              </FormGroup>
              <FormGroup row>
                <Label for='idAddress' sm={2}>Address</Label>
                <Col sm={10}>
                  <Input type='text' name='address' id='idAddress' placeholder='N/A' disabled value={this.state.address} />
                </Col>
              </FormGroup>
              <FormGroup row>
                <Label for='idEmailId' sm={2}>Email ID</Label>
                <Col sm={10}>
                  <Input type='text' name='email' id='idEmailID' placeholder='N/A' value={this.state.emailID} />
                </Col>
              </FormGroup>
              <FormGroup row>
                <Label for='--' sm={2}></Label>
                <Col sm={2}>
                  <Button color="primary" onClick={this.showTable}>Reading Preferences</Button>
                </Col>
              </FormGroup>
              <FormGroup row>
                <Col sm={12}>
                  {this.state.viewTable ? this.readingPreferences : ''}
                </Col>
                <Col sm={12}>
                  {this.state.viewTable ? <Button outline color="secondary" onClick={this.hideTable}>Hide Table</Button> : ''}
                </Col>
              </FormGroup>
            </Form>
          </Col>
        </Row>
      </Container>
    );
  }
}



export default Details;
