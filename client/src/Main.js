import React from 'react';
import styled from 'styled-components';
import {Route} from 'react-router-dom';

import Login from './Login';
import Details from './Details';
import NavbarComponent from './NavbarComponent';

const AppWrapper = styled.div`
  text-align: center;
`

const ContentWrapper = styled.div`
  text-align: center;
  padding-top: 1em;
  margin-top: 1em;
`

const Main = () => (
  <AppWrapper>
    <NavbarComponent />

    <ContentWrapper>
      <Route exact path='/' component={Login}/>
      <Route exact path='/login' component={Login}/>
      <Route exact path='/details' component={Details}/>
    </ContentWrapper>
  </AppWrapper>
);



export default Main;
