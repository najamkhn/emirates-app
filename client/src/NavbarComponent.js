import React from 'react';
import {
  Navbar,
  NavbarBrand,
  Nav,
  NavItem} from 'reactstrap';

import {Link} from 'react-router-dom';

const NavbarComponent = () => (
      <Navbar color='dark' dark expand='md'>
        <NavbarBrand href='/'>Emirates App</NavbarBrand>
          <Nav className='ml-auto' navbar>
            <NavItem>
              <Link to='/login/'>Login</Link>
            </NavItem>
          </Nav>
      </Navbar>
);

export default NavbarComponent;
