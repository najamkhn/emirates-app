const restify = require('restify');
const corsMiddleware = require('restify-cors-middleware')
const jwt = require('jsonwebtoken');
require('dotenv').config();

const users = require('./users.json');
const server = restify.createServer();

const cors = corsMiddleware({
  preflightMaxAge: 5, //Optional
  origins: ['http://localhost:9000']
});

const secret = process.env.SECRETKEY;

server.pre(cors.preflight);
server.use(restify.plugins.queryParser({ mapParams: true }));
server.use(restify.plugins.bodyParser({ mapParams: true }));
server.use(restify.plugins.acceptParser(server.acceptable));
server.use(cors.actual);


server.post('/auth/', (req, res, next) => {
  const body = req.body;
  const user = body.user;
  const password = body.password;

  let result = '';

  for (let x = 0; x < users.length; x++) {
    let entry = users[x];

    if (entry.username == user.trim() && password.trim() == entry.password) {
      result = entry;
      break;
    }
  }

  if (!result) {
    return res.json(401, {error: true});
  }

  const token = jwt.sign(result, secret, { expiresIn: '1h' });
  return res.json(200, { authToken: token });

});

server.listen(9001, function() {
  console.log('%s listening at %s', server.name, server.url);
});
