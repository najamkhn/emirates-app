Emirates App
===

# Server

To run the server, run the following command:

```
yarn start
```

# Client:

To run the client, please run the following commands

```
cd /client; yarn start
```

